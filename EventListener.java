package peru.sugoi.armorstandpurotekutaa;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

@SuppressWarnings("deprecation")
public class EventListener implements Listener {

	@EventHandler
	public void onDamage(EntityDamageEvent e) {
		if (e.getEntity() instanceof ArmorStand && e.getCause() != DamageCause.ENTITY_ATTACK) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onDamageByEntity(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof ArmorStand) {
			e.getEntity().setGravity(false);
			if (e.getDamager() instanceof Player) {
				Player p = ((Player)e.getDamager());
				if (p.hasPermission("armorstandpurotekutaa.kowasiya")) {
					if (p.getGameMode() == GameMode.CREATIVE) {
						if (p.getItemInHand().getType() != Material.GOLD_AXE) {
							e.setCancelled(true);
							p.playSound(p.getLocation(), Sound.BLOCK_NOTE_HARP, 0.4f, 0);
							p.sendTitle(ChatColor.RED + "金のオノじゃないとこわせません！！", ChatColor.RED + "/goldaxe", 5, 10, 5);
							return;
						}else {
							//金のおのでこわしたら
							p.playSound(p.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 0.4f, 2f);
						}
					}else {
						//クリエイティブモードじゃないなら
						p.sendTitle(ChatColor.RED + "クリエイティブモードでこわしてね", ChatColor.RED + "(まじか・・・)", 1, 2, 1);
						e.setCancelled(true);
					}
				}else {
					//こわす権限ないなら
					e.setCancelled(true);
				}
			}else {
				e.setCancelled(true);
				return;
			}
		}
	}

}